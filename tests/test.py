#! /usr/bin/env python3

"""
Tests for pyRSKTools.
"""

from datetime import datetime
import os
import sys
import unittest

from context import pyrsktools

try:
    RSK_FILENAME
except NameError:
    if 'RSK' not in os.environ:
        sys.exit('Please set the "RSK" environment variable with the filename of an RSK to test against.')

    RSK_FILENAME = os.environ['RSK']

class RskTestCase(unittest.TestCase):
    def setUp(self):
        self.rsk = pyrsktools.open(RSK_FILENAME)

    def tearDown(self):
        self.rsk.close()

class TestCanOpenRsk(unittest.TestCase):
    def test_opens_and_closes(self):
        rsk = pyrsktools.open(RSK_FILENAME)
        rsk.close()

    def test_with(self):
        with pyrsktools.open(RSK_FILENAME) as rsk:
            return

class TestSaneMetadata(RskTestCase):
    def test_has_channels(self):
        self.assertTrue(self.rsk.channels is not None,
                        'Missing channels')
        self.assertTrue(len(self.rsk.channels) > 0,
                        'No channels read from RSK')

        for channel in self.rsk.channels:
            self.assertEqual(type(self.rsk.channels[channel].derived), bool)

    def test_has_deployment(self):
        self.assertTrue(self.rsk.deployment is not None,
                        'Missing deployment')
        self.assertEqual(type(self.rsk.deployment),
                         pyrsktools.Deployment,
                        'Wrong instance type')
        self.assertEqual(type(self.rsk.deployment.download_time),
                         datetime,
                         'Wrong instance type')

    def test_has_instrument(self):
        self.assertTrue(self.rsk.instrument is not None,
                        'Missing instrument')
        self.assertEqual(type(self.rsk.instrument),
                         pyrsktools.Instrument,
                         'Wrong instance type')

class TestAccessSamples(RskTestCase):
    def test_has_samples(self):
        samples = self.rsk.samples()
        self.assertTrue(samples is not None)
        sample = next(samples)

    def test_has_numpy_samples(self):
        samples = self.rsk.npsamples()
        self.assertTrue(samples is not None)

class TestAccessProfiles(RskTestCase):
    def test_has_profiles(self):
        profiles = self.rsk.profiles()
        self.assertTrue(profiles is not None)
        profile = next(profiles)

    def test_profiles_have_samples(self):
        profile = next(self.rsk.profiles())
        sample = next(profile.samples())

    def test_profiles_have_numpy_samples(self):
        profile = next(self.rsk.profiles())
        self.assertTrue(profile.npsamples() is not None)

class TestAccessCasts(RskTestCase):
    def test_has_downcasts(self):
        casts = self.rsk.casts(pyrsktools.Region.CAST_DOWN)
        self.assertTrue(casts is not None)
        casts = next(casts)

    def test_has_upcasts(self):
        casts = self.rsk.casts(pyrsktools.Region.CAST_UP)
        self.assertTrue(casts is not None)
        casts = next(casts)

    def test_casts_have_samples(self):
        cast = next(self.rsk.casts(pyrsktools.Region.CAST_UP))
        sample = next(cast.samples())

    def test_casts_have_numpy_samples(self):
        cast = next(self.rsk.casts(pyrsktools.Region.CAST_UP))
        self.assertTrue(cast.npsamples() is not None)

if __name__ == '__main__':
    unittest.main()
