#! /usr/bin/env python3

"""
Context setup for pyRSKTools tests.
"""

import os
import sys
# We want to be able to run tests from within the `tests` directory...
sys.path.insert(0, os.path.abspath('..'))
# ...and from within the project root.
sys.path.insert(1, os.path.abspath('.'))

import pyrsktools
