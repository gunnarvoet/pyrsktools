package: source
	python3 setup.py bdist_wheel

install: source
	pip3 install -e .[NumPy]

source: pyrsktools/__init__.py \
        pyrsktools/channeltypes.py \
        setup.py \
        MANIFEST.in \
        README.rst \
        CHANGES.rst \
        LICENSE.txt

upload: package
	twine upload dist/pyRSKTools-*-py3-none-any.whl

test: pyrsktools/__init__.py tests/test.py
	python3 -m unittest discover -v tests/

.PHONY: clean
clean:
	# Clean up build artifacts.
	-rm -R build dist pyRSKTools.egg-info
	# Clean up bytecode leftovers.
	find . -type f -name '*.pyc' -print0 | xargs -0 rm
	find . -type d -name '__pycache__' -print0 | xargs -0 rmdir
