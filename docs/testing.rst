=======
Testing
=======

To test the project,
set the ``RSK`` environment variable
to point at an RSK file:

.. code-block:: shell

    $ export RSK=~/Downloads/some_rsk.rsk

Then run
the unit tests:

.. code-block:: shell

    $ make test
